"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import cv2
from src.methods.dai.function import norm_std_img
from src.methods.dai.function import filter



def run_reconstruction(pan: np.ndarray, ms1: np.ndarray, ms2: np.ndarray)-> np.ndarray:
    """Performs ata fusion of the three acquisitions.
    
    Args:
        pan (np.ndarray): Panchromatic image.
        ms1 (np.ndarray): Mid resolution multispectral image.
        ms2 (np.ndarray): Low resolution multispectral image.

    Returns:
        np.ndarray: Full spatial and spectral iamge.
    """

    
    #denoising
    pan,ms1,ms2 = filter(pan,ms1,ms2)


    # Performing the reconstruction.
    ms2_upscaled = cv2.resize(ms2,(ms1.shape[1],ms1.shape[0]),interpolation = cv2.INTER_CUBIC)
    
    N=ms2.shape[2]

    w=1/N
    I1=np.zeros((ms1.shape[0],ms1.shape[1]))
    I2=np.zeros(pan.shape)

    for i in range(N):
        I1+=ms2_upscaled[:,:,i]*w
 
    S1=np.zeros(ms2_upscaled.shape)
    
    ms1_upscaled_norm = np.zeros(ms2_upscaled.shape)

    for k in range(N):
        for i in range(ms1.shape[2]):
            S1[:,:,k]+=ms2_upscaled[:,:,k]+np.divide(ms2_upscaled[:,:,k],I1)*(ms1[:,:,i]-I1)
    S1=S1/ms1.shape[2]


    S1_upscaled = cv2.resize(S1,(pan.shape[1],pan.shape[0]),interpolation = cv2.INTER_CUBIC)

    for i in range(N):
        I2+=S1_upscaled[:,:,i]*w

    pan_norm=norm_std_img(pan,I2)

    S2=np.zeros(S1_upscaled.shape)

    for k in range(N):
        S2[:,:,k]=S1_upscaled[:,:,k]+np.divide(S1_upscaled[:,:,k],I2)*(pan_norm-I2)
        
    return 0.32*S1_upscaled+0.68*S2


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
